@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="film-img" >

                </div>
            </div>
            <div class="col-md-6">
                <div class="film-details">
                    <div class="film-name">
                    </div>
                    <div class="film-rating">
                    </div>
                    <div class="film-price">
                    </div>
                    <div class="film-release-date">
                    </div>
                    <div class="film-country">
                    </div>
                    <div class="film-description">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel-default">

                    <div class="panel-body">
                        <div class="panel-title">Comments:</div>


                        <div class="comments">

                        </div>

                        @if (Auth::guest())
                            <p><a href="/register">Register</a> or <a href="/login">login</a> to comment</p>
                        @else

                            <form method="POST" action="/addcomment">
                                {{ csrf_field() }}
                                <div class="panel-title">Post a Comment</div>
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input type="text" class="form-control" name="name" id="name" placeholder="Place your name">
                                </div>
                                <div class="form-group">
                                    <label for="body">Comments</label>
                                    <textarea class="form-control"  name="body" rows="3" placeholder="Place your comments."></textarea>
                                </div>
                                <input type="hidden" name="userid" value="{{ Auth::user()->id }}">
                                <input type="hidden" name="filmid" value="">
                                <button type="submit" class="btn btn-default">Submit</button>
                            </form>

                        @endif



                    </div>

                </div>

            </div>
        </div>

    </div>
    </div>


    <script>
        var pathArray = window.location.pathname.split('/');
        console.log(pathArray);

        $.getJSON("http://127.0.0.1:8000/api/films/" + pathArray[2], function (data) {
            $('.film-img').css('background-image', 'url(' + data[0]['photo'] + ')');
            $('.film-name').append('<p>' + data[0]['name'] + '</p>');
            $('.film-rating').append(evaluateRating(data[0]['rating'] ));
            $('.film-price').append('<p>$' + data[0]['ticket_price'] + '</p>');
            $('.film-release-date').append('<p>' + data[0]['release_date'] + '</p>');
            $('.film-country').append('<p>' + data[0]['country'] + '</p>');
            $('.film-description').append('<p>' + data[0]['description'] + '</p>');
            $("input[name=filmid]").val(data[0]['id']);


            var comments = data[0]['comments'];
            comments.forEach(function (comment) {
                var htmlItem = '<div class="comment">\n' +
                    '                                <div class="user">\n' +
                    '                                    <h4>'+comment['name']+' at '+comment['created_at']+'</h4>\n' +
                    '                                </div>\n' +
                    '                                <div class="message">\n' +
                    '                                    <p>'+comment['body']+'</p>\n' +
                    '                                </div>\n' +
                    '                            </div>';

                $('.comments').append(htmlItem);

            });

        });


        function evaluateRating(rating) {
            if (parseInt(rating) == 1) {
                ratingDiv = '<div class="film-rating">\n' +
                    '                                <span class="fa fa-star checked"></span>\n' +
                    '                                <span class="fa fa-star"></span>\n' +
                    '                                <span class="fa fa-star"></span>\n' +
                    '                                <span class="fa fa-star"></span>\n' +
                    '                                <span class="fa fa-star"></span>\n' +
                    '                            </div>';
            }
            if (parseInt(rating) == 2) {
                ratingDiv = '<div class="film-rating">\n' +
                    '                                <span class="fa fa-star checked"></span>\n' +
                    '                                <span class="fa fa-star checked"></span>\n' +
                    '                                <span class="fa fa-star"></span>\n' +
                    '                                <span class="fa fa-star"></span>\n' +
                    '                                <span class="fa fa-star"></span>\n' +
                    '                            </div>';

            }
            if (parseInt(rating) == 3) {
                ratingDiv = '<div class="film-rating">\n' +
                    '                                <span class="fa fa-star checked"></span>\n' +
                    '                                <span class="fa fa-star checked"></span>\n' +
                    '                                <span class="fa fa-star checked"></span>\n' +
                    '                                <span class="fa fa-star"></span>\n' +
                    '                                <span class="fa fa-star"></span>\n' +
                    '                            </div>';

            }
            if (parseInt(rating) == 4) {
                ratingDiv = '<div class="film-rating">\n' +
                    '                                <span class="fa fa-star checked"></span>\n' +
                    '                                <span class="fa fa-star checked"></span>\n' +
                    '                                <span class="fa fa-star checked"></span>\n' +
                    '                                <span class="fa fa-star checked"></span>\n' +
                    '                                <span class="fa fa-star"></span>\n' +
                    '                            </div>';

            }
            if (parseInt(rating) == 5) {
                ratingDiv = '<div class="film-rating">\n' +
                    '                                <span class="fa fa-star checked"></span>\n' +
                    '                                <span class="fa fa-star checked"></span>\n' +
                    '                                <span class="fa fa-star checked"></span>\n' +
                    '                                <span class="fa fa-star checked"></span>\n' +
                    '                                <span class="fa fa-star checked"></span>\n' +
                    '                            </div>';


            }

            return ratingDiv;

        }
    </script>
@endsection