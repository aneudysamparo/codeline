@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Add New Film</div>

                    <div class="panel-body">
                        @if (Auth::guest())
                            <p><a href="/register">Register</a> or <a href="/login">login</a> to comment</p>
                        @else

                            <form method="POST" action="/addfilm">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input type="text" class="form-control" name="name" id="name"
                                           placeholder="Fast 2">
                                </div>
                                <div class="form-group">
                                    <label for="description">Description</label>
                                    <textarea class="form-control" name="description" rows="3"
                                              placeholder="Lorem input"></textarea>
                                </div>

                                <div class="form-group">
                                    <label for="release_date">Release Date</label>
                                    <input type="text" class="form-control" name="release_date" id="release_date"
                                           placeholder="mm/dd/yyyy">
                                </div>
                                <div class="form-group">
                                    <label for="rating">Rating</label>
                                    <input type="text" class="form-control" name="rating" id="rating" value="4"
                                           placeholder="1-5">
                                </div>

                                <div class="form-group">
                                    <label for="ticket_price">Ticket Price</label>
                                    <input type="text" class="form-control" name="ticket_price" id="ticket_price"
                                           placeholder="$0.00">
                                </div>
                                <div class="form-group">
                                    <label for="country">Country</label>
                                    <input type="text" class="form-control" name="country" id="country"
                                           placeholder="Australia">
                                </div>
                                <div class="form-group">
                                    <label for="photo">Photo URL</label>
                                    <input type="text" class="form-control" name="photo" id="photo"
                                           placeholder="http://domain.com/photo.jpg">
                                </div>

                                <input type="hidden" name="userid" value="{{ Auth::user()->id }}">
                                <button type="submit" class="btn btn-default">Submit</button>
                            </form>

                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection