@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <div class="row" id="films">

                </div>

            </div>
        </div>
    </div>
    </div>
    <script>
        $.getJSON("http://127.0.0.1:8000/api/films", function (data) {

            var htmlFilms = $('#films');

            data[0].forEach(function (item) {

                var htmlFilmItem = '<div class="col-lg-3 col-md-3 col-sm-6" id="film-item">\n' +
                    '                        <div class=" panel panel-default">\n' +
                    '                            <div class="film-img"\n' +
                    '                                 style="background-image: url(' + item['photo'] + ')">\n' +
                    '                            </div>\n' +
                    '                            <div class="panel-body text-center">\n' +
                    '                                <div class="film-name">' + item['name'] + '</div>\n' +
                    evaluateRating(item['rating']) +
                    '<div class="film-genre">\n' +
                    getGenres(item['genres']) +
                    ' </div>\n' +
                    '                                <div class="film-price">\n' +
                    '                                    <span>$' + item['ticket_price'] + '</span>\n' +
                    '                                </div>\n' +
                    '                                <div class="film-release-date">\n' +
                    '                                    <span>' + item['release_date'] + '</span>\n' +
                    '                                </div>\n' +
                    '                                <div class="film-country">\n' +
                    '                                    <span>' + item['country'] + '</span>\n' +
                    '                                </div>\n' +
                    '                                <div class="film-btn">\n' +
                    '                                    <a class="btn btn-primary" href="/films/' + item['slug'] + '">View Details</a>\n' +
                    '                                </div>\n' +
                    '                            </div>\n' +
                    '                        </div>\n' +
                    '                    </div>';


                htmlFilms.append(htmlFilmItem);
            });


            console.log(data[0]);

            function getGenres(genres) {

                console.log(genres);
                genresDiv = "";
                if (genres.length > 0 && genres.length <= 1) {
                    genres.forEach(function (item) {
                        genresDiv += '<span>' + item + '</span>\n';
                    });
                }
                if (genres.length >= 2) {
                    genres.forEach(function (item) {
                        genresDiv += '<span>' + item + ',</span>\n';
                    });
                }
                return genresDiv;
            }

            function evaluateRating(rating) {
                if (parseInt(rating) == 1) {
                    ratingDiv = '<div class="film-rating">\n' +
                        '                                <span class="fa fa-star checked"></span>\n' +
                        '                                <span class="fa fa-star"></span>\n' +
                        '                                <span class="fa fa-star"></span>\n' +
                        '                                <span class="fa fa-star"></span>\n' +
                        '                                <span class="fa fa-star"></span>\n' +
                        '                            </div>';
                }
                if (parseInt(rating) == 2) {
                    ratingDiv = '<div class="film-rating">\n' +
                        '                                <span class="fa fa-star checked"></span>\n' +
                        '                                <span class="fa fa-star checked"></span>\n' +
                        '                                <span class="fa fa-star"></span>\n' +
                        '                                <span class="fa fa-star"></span>\n' +
                        '                                <span class="fa fa-star"></span>\n' +
                        '                            </div>';

                }
                if (parseInt(rating) == 3) {
                    ratingDiv = '<div class="film-rating">\n' +
                        '                                <span class="fa fa-star checked"></span>\n' +
                        '                                <span class="fa fa-star checked"></span>\n' +
                        '                                <span class="fa fa-star checked"></span>\n' +
                        '                                <span class="fa fa-star"></span>\n' +
                        '                                <span class="fa fa-star"></span>\n' +
                        '                            </div>';

                }
                if (parseInt(rating) == 4) {
                    ratingDiv = '<div class="film-rating">\n' +
                        '                                <span class="fa fa-star checked"></span>\n' +
                        '                                <span class="fa fa-star checked"></span>\n' +
                        '                                <span class="fa fa-star checked"></span>\n' +
                        '                                <span class="fa fa-star checked"></span>\n' +
                        '                                <span class="fa fa-star"></span>\n' +
                        '                            </div>';

                }
                if (parseInt(rating) == 5) {
                    ratingDiv = '<div class="film-rating">\n' +
                        '                                <span class="fa fa-star checked"></span>\n' +
                        '                                <span class="fa fa-star checked"></span>\n' +
                        '                                <span class="fa fa-star checked"></span>\n' +
                        '                                <span class="fa fa-star checked"></span>\n' +
                        '                                <span class="fa fa-star checked"></span>\n' +
                        '                            </div>';


                }

                return ratingDiv;

            }

        });
    </script>
@endsection