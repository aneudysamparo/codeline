<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('films');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/films', 'FilmController@webFilms');

Route::get('/films/create', 'FilmController@webFilmCreate');

Route::post('/addfilm', 'FilmController@addFilm');

Route::post('/addcomment', 'FilmController@addComment');

Route::get('/films/{id}', 'FilmController@webFilmById');

Route::post('/films/{id}', 'FilmController@webAddComment');
