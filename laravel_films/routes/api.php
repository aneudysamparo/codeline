<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/test', function () {
    return response([1, 2, 3, 4, 5, 6, 7, 8], 200);
})->middleware('auth:api');

Route::post('login', 'AuthController@login');
Route::post('signup', 'AuthController@signup');





//Route::get('films/{id}', 'FilmController@getById');
Route::get('films/{slug}', 'FilmController@getBySlug');
Route::get('films', 'FilmController@getAll');

Route::post('films/{id}/','FilmController@addComment')->middleware('auth:api');


Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');
