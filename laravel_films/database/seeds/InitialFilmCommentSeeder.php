<?php

use Illuminate\Database\Seeder;

class InitialFilmCommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // '', 'ticket_price', 'country', 'photo', 'slug'];
        for ($i=0; $i<9; $i++){
            $name = 'Fast 3'.$i;
            DB::table('films')->insert([
                'name' => $name,
                'description' => 'Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                'release_date' => '12/12/203'.$i,
                'rating' => '4',
                'ticket_price' => '4'.$i,
                'country' => 'United States',
                'photo' =>'https://dummyimage.com/600x400/000/fff.jpg&text=fast3'.$i,
                'slug' => $this->slugify($name)
            ]);
        }
    }



    protected function slugify($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }
}
