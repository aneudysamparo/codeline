<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;
use Config;
use Validator;
use Illuminate\Support\Facades\Route;

class AuthController extends Controller


{
    /**
     * Get Token.
     */
    protected function GetToken(Request $request, $email, $password)
    {

        $request->request->add([
            'username' => $email,
            'password' => $password,
            'grant_type' => 'password',
            'client_id' => 2,
            'client_secret' => 'IhltRbvIhb5cX5vQZg83jbRCZ1OoDGdfkVCcb8wD',
            'scope' => '*'
        ]);

        $tokenRequest = Request::create(
            'http://127.0.0.1:8000/oauth/token',
            'post'
        );
        return Route::dispatch($tokenRequest)->getContent();
    }


    /**
     * Login Email and Password Account.
     */
    public function login(Request $request)
    {
        $email = $request->input('email');
        $password = $request->input('password');


        $user = User::where('email', '=', $email)->first();

        if (!$user) {
            return response()->json(['message' => 'Wrong email and/or password'], 401);
        }

        if (Hash::check($password, $user->password)) {
            unset($user->password);

            return $this->GetToken($request, $email, $password);
        } else {
            return response()->json(['message' => 'Wrong email and/or password'], 401);
        }
    }

    /**
     * Create Email and Password Account.
     */
    public function signup(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['message' => $validator->messages()], 400);
            exit();
        }

        $user = new User;
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->password = Hash::make($request->input('password'));
        $user->save();

        return $this->GetToken($request, $request->input('email'), $request->input('password'));
    }
}
