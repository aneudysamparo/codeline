<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Film;
use App\Genre;
use App\GenreFilm;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class FilmController extends Controller
{


    public function webFilms()
    {
        return view('films');
    }

    public function webFilmById()
    {
        return view('film');
    }

    public function webFilmCreate()
    {
        return view('create');
    }

    public function addFilm()
    {

        $userid = Auth::user()->id;
        if (!$userid) {
            return redirect('login');
        }

        $input = Input::only('name', 'description', 'release_date', 'rating', 'ticket_price', 'country', 'photo');
        $film = new Film();
        $film->name = $input['name'];
        $film->description = $input['description'];
        $film->release_date = $input['release_date'];
        $film->rating = $input['rating'];
        $film->ticket_price = $input['ticket_price'];
        $film->country = $input['country'];
        $film->photo = $input['photo'];
        $film->slug = $this->slugify($input['name']);
        $film->save();

        return redirect('films/' . $film->id);

    }

    public function addComment()
    {
        $userid = Auth::user()->id;
        if (!$userid) {
            return redirect('login');
        }


        $input = Input::only('name', 'body', 'userid', 'filmid');

        $comment = new Comment();
        $comment->name = $input['name'];
        $comment->body = $input['body'];
        $comment->film_id = $input['filmid'];
        $comment->user_id = $userid;
        $comment->save();

        return redirect('films/' . $input['filmid']);
    }

    public function getBySlug($slug)
    {

        $film = Film::where('slug', '=', $slug)->firstOrFail();


        $genreFilms = GenreFilm::where('film_id', '=', $film['id'])->get();
        $comments = Comment::where('film_id', '=', $film->id)->get();

        $_filmGenres = array();
        foreach ($genreFilms as $genreFilm) {
            if ($film['id'] == $genreFilm['film_id']) {
                $_genre = Genre::find($genreFilm['genre_id']);
                $_filmGenres[] = $_genre->name;
            }
        }

        $film['genres'] = $_filmGenres;
        $film['comments'] = $comments;

        return response()->json([$film]);
    }


    public function getById(Request $request, $id)
    {
        $film = Film::find($id);

        $genreFilms = GenreFilm::where('film_id', '=', $film['id'])->get();
        $comments = Comment::where('film_id', '=', $film['id'])->get();

        $_filmGenres = array();
        foreach ($genreFilms as $genreFilm) {
            if ($film['id'] == $genreFilm['film_id']) {
                $_genre = Genre::find($genreFilm['genre_id']);
                $_filmGenres[] = $_genre->name;
            }
        }

        $film['genres'] = $_filmGenres;
        $film['comments'] = $comments;

        return response()->json([$film]);
    }


    public function getAll()
    {

        $films = Film::all();
        $genreFilms = GenreFilm::all();

        foreach ($films as $film) {

            $_filmGenres = array();
            foreach ($genreFilms as $genreFilm) {
                if ($film['id'] == $genreFilm['film_id']) {
                    $_genre = Genre::find($genreFilm['genre_id']);
                    $_filmGenres[] = $_genre->name;
                }
            }

            $comments = Comment::where('film_id', '=', $film['id'])->get();

            $film['genres'] = $_filmGenres;
            $film['comments'] = $comments;
        }
        return response()->json([$films]);
    }

    protected function slugify($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }
}
