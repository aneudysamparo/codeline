<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Film extends Model
{
    protected $fillable = ['name', 'description', 'release_date', 'rating', 'ticket_price', 'country', 'photo', 'slug'];


    public function genres()
    {
        return $this->hasMany(Genre::class);

    }

}
