<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GenreFilm extends Model
{

    protected $fillable = ['film_id', 'genre_id'];
}
