angular.module('controllers', [])

    .controller('AppCtrl', function ($scope, WeatherAPI, $state, $window) {

        $scope.data = "hola data";
        $scope.cities = [];
        var mainCities = [9807, 560743, 565346, 44418, 638242, 2344116];

        mainCities.forEach(function (city) {
            WeatherAPI.getByWoeid(city).then(function (res) {
                console.log(res.data)
                $scope.cities.push(res.data);
            });
        });

        console.log($scope.cities);
        $scope.citySearch = "";

        $scope.doSearch = function (keyword) {


            if($scope.citySearch == ""){
                return;
            }
            else {
                $window.location.href = '#!/search/'+keyword;
            }
        }


    })
    .controller('SearchCtrl', function ($scope, $stateParams, WeatherAPI) {
        WeatherAPI.getByCity($stateParams.keyword).then(function (res) {
            //console.log(res.data);
            WeatherAPI.getByWoeid(res.data[0].woeid).then(function (response) {
                $scope.city = response.data;
                //console.log(response.data);
            });

        });

    })
    .controller('CityCtrl', function ($scope, $stateParams, WeatherAPI) {

        WeatherAPI.getByWoeid($stateParams.woeid).then(function (res) {
            console.log(res.data);
            $scope.city = res.data;
        });


    });