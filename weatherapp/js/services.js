angular.module('services', [])

    .factory('WeatherAPI', function($http) {

        var baseUrl = {
            search: "http://192.168.64.2/weather.php?command=search&keyword=",
            location: "http://192.168.64.2/weather.php?command=location&woeid="
        };


        return {
            getByWoeid: function(woeid) {
                return $http.get(baseUrl.location+woeid);
            },
            getByCity:function (keyword) {
                return $http.get(baseUrl.search+keyword);
            }
        };
    });