angular.module("myApp", ["ui.router", "controllers", "services"])

    .config(function ($stateProvider, $urlRouterProvider) {
        $stateProvider

            .state('home', {
                url: '/',
                templateUrl: 'templates/main.html',
                controller: 'AppCtrl'
            })
            .state('search', {
                url: '/search/:keyword',
                templateUrl: 'templates/search.html',
                controller: 'SearchCtrl'
            })
            .state('city', {
                url: '/weather/:woeid',
                templateUrl: 'templates/single-city.html',
                controller: 'CityCtrl'
            });
        $urlRouterProvider.otherwise('/');
    })
    .component("weather", {
        templateUrl: "templates/weatherComponent.html",
        bindings: {data: '='},
        controller: function(){
            //this.myName = 'Alain';
        }
    });