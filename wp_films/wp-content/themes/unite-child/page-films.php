<?php
/**
 * The template for displaying Archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 * Template Name: Films Template
 * @package unite-child
 */

get_header(); ?>

<section id="primary" class="content-area col-sm-12 col-md-8 <?php echo of_get_option('site_layout'); ?>">
    <main id="main" class="site-main" role="main">

        <?php if (have_posts()) : ?>

            <header class="page-header">
                <?php
                the_archive_title('<h1 class="page-title">', '</h1>');
                the_archive_description('<div class="taxonomy-description">', '</div>');
                ?>
            </header><!-- .page-header -->

            <?php
            /* Start the Loop */
            // set up or arguments for our custom query
            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
            $query_args = array(
                'post_type' => 'films',
                //'category_name' => 'tutorials',
                'posts_per_page' => 8,
                'post_status' => 'publish',
                'paged' => $paged,/*
                        'meta_key'      => 'condicion',
                        'meta_value'    => array('Usado', 'Nuevo')*/

            );
            // create a new instance of WP_Query
            $the_query = new WP_Query($query_args);
            if ($the_query->have_posts()) : while ($the_query->have_posts()) : $the_query->the_post(); // run the loop
                ?>
                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <header class="entry-header page-header">

                        <?php
                        if (of_get_option('single_post_image', 1) == 1) :
                            the_post_thumbnail('unite-featured', array('class' => 'thumbnail'));
                        endif;
                        ?>

                        <h1 class="entry-title "><?php the_title(); ?></h1>

                        <div class="entry-meta">
                            <?php unite_posted_on(); ?>
                        </div><!-- .entry-meta -->
                    </header><!-- .entry-header -->

                    <div class="entry-content">
                        <?php the_content(); ?>
                        <?php
                        wp_link_pages(array(
                            'before' => '<div class="page-links">' . __('Pages:', 'unite'),
                            'after' => '</div>',
                        ));
                        ?>
                    </div><!-- .entry-content -->

                    <footer class="entry-meta">
                        <?php
                        /* translators: used between list items, there is a space after the comma */
                        $category_list = get_the_category_list(__(', ', 'unite'));

                        /* translators: used between list items, there is a space after the comma */
                        $tag_list = get_the_tag_list('', __(', ', 'unite'));

                        if (!unite_categorized_blog()) {
                            // This blog only has 1 category so we just need to worry about tags in the meta text
                            if ('' != $tag_list) {
                                $meta_text = '<i class="fa fa-folder-open-o"></i> %2$s. <i class="fa fa-link"></i> <a href="%3$s" rel="bookmark">permalink</a>.';
                            } else {
                                $meta_text = '<i class="fa fa-link"></i> <a href="%3$s" rel="bookmark">permalink</a>.';
                            }

                        } else {
                            // But this blog has loads of categories so we should probably display them here
                            if ('' != $tag_list) {
                                $meta_text = '<i class="fa fa-folder-open-o"></i> %1$s <i class="fa fa-tags"></i> %2$s. <i class="fa fa-link"></i> <a href="%3$s" rel="bookmark">permalink</a>.';
                            } else {
                                $meta_text = '<i class="fa fa-folder-open-o"></i> %1$s. <i class="fa fa-link"></i> <a href="%3$s" rel="bookmark">permalink</a>.';
                            }

                        } // end check for categories on this blog

                        printf(
                            $meta_text,
                            $category_list,
                            $tag_list,
                            get_permalink()
                        );
                        ?>

                        <?php edit_post_link(__('Edit', 'unite'), '<i class="fa fa-pencil-square-o"></i><span class="edit-link">', '</span>'); ?>
                        <?php unite_setPostViews(get_the_ID()); ?>
                        <hr class="section-divider">
                    </footer><!-- .entry-meta -->
                </article><!-- #post-## -->

            <?php endwhile; ?>

                <?php unite_paging_nav(); ?>

            <?php else : ?>

                <?php get_template_part('content', 'none'); ?>

            <?php endif; ?>
        <?php endif; ?>

    </main><!-- #main -->
</section><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
