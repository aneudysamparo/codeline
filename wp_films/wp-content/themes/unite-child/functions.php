<?php


/*
	Getting Parent's styles
*/
add_action('wp_enqueue_scripts', 'enqueue_parent_styles');

function enqueue_parent_styles()
{
    wp_enqueue_style('parent-style', get_template_directory_uri() . '/style.css');
}


/*
	Register a new custom post type for films
*/

function post_type_films()
{
    $labels = array(
        'name' => _x('Films', 'post type general name'),
        'singular_name' => _x('Film', 'post type singular name'),
        'menu_name' => _x('Films', 'admin menu'),
        'name_admin_bar' => _x('Add Film', 'add new on admin bar'),
        'add_new' => _x('Add Film', 'Film'),
        'add_new_item' => __('Add Film'),
        'new_item' => __('New Films'),
        'edit_item' => __('Edit Film'),
        'view_item' => __('All Film'),
        'all_items' => __('All Films'),
        'search_items' => __('Search for Film'),
        'parent_item_colon' => __('Order:'),
        'not_found' => __('No films found.'),
        'not_found_in_trash' => __('No films found in trash.'),
    );

    $args = array(

        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'films'),
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => 5,
        'menu_icon' => 'dashicons-admin-media',
        'supports' => array('title', 'editor', 'thumbnail'),

    );
    register_post_type('films', $args);
}

add_action('init', 'post_type_films');
add_theme_support('post-thumbnails');

function genre_taxonomy()
{
    register_taxonomy(
        'genre',
        array(
            'hierarchical' => true,
            'query_var' => true,
            'rewrite' => array(
                'slug' => 'genre',
                'with_front' => false
            )
        )
    );
}

add_action('init', 'genre_taxonomy');

function country_taxonomy()
{
    register_taxonomy(
        'country',
        'films',
        array(
            'hierarchical' => true,
            'label' => 'Film Country',
            'query_var' => true,
            'rewrite' => array(
                'slug' => 'country',
                'with_front' => false
            )
        )
    );
}

add_action('init', 'country_taxonomy');

function year_taxonomy()
{
    register_taxonomy(
        'year',
        'films',
        array(
            'hierarchical' => true,
            'label' => 'Film Year',
            'query_var' => true,
            'rewrite' => array(
                'slug' => 'year',
                'with_front' => false
            )
        )
    );
}

add_action('init', 'year_taxonomy');

function actors_taxonomy()
{
    register_taxonomy(
        'actors',
        'films',
        array(
            'hierarchical' => true,
            'label' => 'Film Actors',
            'query_var' => true,
            'rewrite' => array(
                'slug' => 'actors',
                'with_front' => false
            )
        )
    );
}

add_action('init', 'actors_taxonomy');


add_shortcode('display_films', 'display_films_function');

function display_films_function()
{
    $args = array(
        'post_type' => 'films',
        'post_status' => 'publish'
    );

    $string = '';
    $query = new WP_Query($args);
    if ($query->have_posts()) {
        $string .= '<ul class="films">';
        while ($query->have_posts()) {
            $query->the_post();
            $string .= '<li class="film-item"><a href="' . get_permalink() . '">' . get_the_title() . '</a></li>';
        }
        $string .= '</ul>';
    }
    wp_reset_postdata();
    return $string;
}














